﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDisplay : MonoBehaviour
{
    //contains list of game icons 
    //shown in unity from public tag
    //square brackets indicate game objects
    public GameObject[] healthIcons;

    //this will contrain player health component
    //s we can ask it for the players health
    PlayerHealth player;


    // Start is called before the first frame update
    void Start()
    {
        //search scene for the object with player health attached
        //then store the PlayerHealth object in our player variable
        player = FindObjectOfType<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        // Create variable to keep track of what each health icon is worth and what health amount we are currently on
        int iconHealth = 0;

        //Go through each icon in the list
        //we will do everything inside the brackets for each item in the list 
        // For each step in the loop we will store the current list item in the icon variable 
        foreach (GameObject icon in healthIcons)
        {
            //each icon has 1 more health than the last
            //then we get the current health and add to it
            //then store the result back into the iconHealth variable
            iconHealth = iconHealth + 1;
            
            if (player.GetHealth() >= iconHealth)
            {
                //this turns the icon on
                icon.SetActive(true);
            }
            else
            {
                //this turns the icon off
                icon.SetActive(false);

            }
        }
    }
}
