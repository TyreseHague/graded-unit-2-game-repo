﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{

    //character variables
    Rigidbody2D character; //sets rigidbody2d as variable
    Animator animator;
    SpriteRenderer spriterenderer;

    //movement variables
    public float speed = 5f; //sets movement speed as public variale
    
    //jump variables
    public float jumpForce = 200f; //stores the hieght value of a jump
    bool isGrounded; //booleanto see  if object is grounded
    public float checkRadius; //sets the radius
    public LayerMask whatIsGrounded; //sets the groundcheck as an object
    public Transform groundCheck; //sets the groundcheck as an object

    //firing varoables
    public GameObject projectilePrefab; //declares the prejectile prefab
    public Vector2 projectileVelocity; //sets the projectile velocity 
    public float shootdelay; //time between projectiles
    public Transform projectileSpawnPos; //finds projectile spawn position

    bool isFacingLeft; //checks if the player is facing left
    bool isAimingUp; //checks if the player is aiming up


    void Start() //called at the start of the game
    {
        animator = GetComponent<Animator>(); //sets animator
        character = GetComponent<Rigidbody2D>(); //gets the character as a rigidbody2D
        spriterenderer = GetComponent<SpriteRenderer>();
    }


    private void Update()
    {
        if (Input.GetButtonDown("Fire2")) //gets fire input (p)
        {
            //Use instantiate to clone projectile and keep the result in our variable 
            GameObject b = Instantiate(projectilePrefab); //gameobject = the prejectile prefab
            b.GetComponent<PlayerDamage>().StartShoot(isFacingLeft, isAimingUp); //transfers the valules of the projectile
            b.transform.position = projectileSpawnPos.transform.position; 


        }
    }



    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGrounded);



        if (Input.GetKey("d") || Input.GetKey("right")) //gets 
        {
            character.velocity = new Vector2(speed, character.velocity.y);
            isFacingLeft = false; //the player isnt facing left
            isAimingUp = false; //the player isnt aiming up
            if (isGrounded == true)
                animator.Play("RunRight");
            transform.localScale = new Vector3(0.35f, 0.35f, 0.35f);

        }

        else if (Input.GetKey("a") || Input.GetKey("left"))
        {
            character.velocity = new Vector2(-speed, character.velocity.y);
            isFacingLeft = true;
            isAimingUp = false; //the player isnt aiming up
            if (isGrounded == true)
                animator.Play("RunRight");
            transform.localScale = new Vector3(-0.35f, 0.35f, 0.35f);
        }

        else if (Input.GetKey("w")) //gets input
        {
            if (isGrounded == true)
                isAimingUp = true; //sets it so the projectiles velocity will be upward instead of normal
                animator.Play("AimUp"); //plays animation
            character.velocity = new Vector2(0, character.velocity.y); //stops the character sliding while crouching
        }

        else if (Input.GetKey("s"))
        {
            if (isGrounded == true)
                animator.Play("Crouch"); //plays animation
            character.velocity = new Vector2(0, character.velocity.y); //stops the character sliding while crouching
        }

        else
        {
            if (isGrounded == true)
                animator.Play("Idol"); //returns player to idle when no button is pressed
            character.velocity = new Vector2(0, character.velocity.y); //stops any velocity, this makes it so that the player won't skate across the ground after a button press
            isAimingUp = false;
        }

        if (Input.GetKey("o") && isGrounded == true) //gets the input and checks if the player is grounded
        {
            character.velocity = new Vector2(character.velocity.x, jumpForce); //sets jump movement speed
            animator.Play("Jump"); //plays jump animation
        }
    }
}
