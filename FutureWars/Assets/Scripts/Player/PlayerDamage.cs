﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour


{
    public int damage; //damage as a public variable
    public float speed; //speed as a public variable


    public void StartShoot(bool isFacingLeft, bool isAimingUp) //brings in if the player is facing left or aiming up
    {
        Rigidbody2D rb2d = GetComponent<Rigidbody2D>();

        if (isFacingLeft) //if the player is facing left
        {
            rb2d.velocity = new Vector2(-speed, 0); //sends projectile backwards
        }
        else
        {
            rb2d.velocity = new Vector2(speed, 0); //sends projectile forwards
        }

        if (isAimingUp == true) //if the player is facing up
        {
            rb2d.velocity = new Vector2(0, speed); //sends projectile upwards
        }
    }


    // CONDITION: When the projectile hits an enemy
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        // Checks if object has the "Enemy" tag
        if (otherCollider.CompareTag("Enemy") == true) //if the projecile collides with something with the enemy tag
        {
            otherCollider.gameObject.GetComponent<EnemyHealth>().hurtEnemy(damage); 
            Destroy(gameObject); //destroys projectile

        }
        else if (otherCollider.CompareTag("Ground") == true) //if the projecile collides with something with the ground tag
        {
            Destroy(gameObject); //destroys projectile

        }
    }

 
}
