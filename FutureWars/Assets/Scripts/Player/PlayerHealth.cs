﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    //sets the players starting health value
    public int playerStartingHealth;

    //sets what the health currently is
    int playerCurrentHealth;

    //sets a game over scene 
    public string gameOverScene;

    //this is called before the start function
    void Awake()
    {
        //make current health equal to starting health
        playerCurrentHealth = playerStartingHealth;
    }
    //will change current health 
    //kills player at 0 health
    public void changeHealth(int changeAmount)
    {
        //takes current health and adds the change
        //then sets the current health to the old game
        playerCurrentHealth = playerCurrentHealth + changeAmount;

        //keeps current health between 0 and 3
        playerCurrentHealth = Mathf.Clamp(playerCurrentHealth, 0, playerStartingHealth);

        //if your health is at 0 you die
        if (playerCurrentHealth == 0)
        {
            Kill();
        }
    }

    // it must be a pubic void so other scripts can use it
    public void Kill()
    {
        // This will destroy the gameObject that this script is attached to 
        Destroy(gameObject);

        // Load the gameover scene
        SceneManager.LoadScene(gameOverScene);
    }

    //this allows you to access the currentHealth variable in the HealthDisplay script
    public int GetHealth()
    {
        return playerCurrentHealth; //returns the player health
    }

}
