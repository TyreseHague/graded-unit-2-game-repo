﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    //sets enemy damage
    //int = whole number
    public int hazardDamage;

    //built in unity function forhandling collision
    //made for when the hazard touches an object
    void OnCollisionEnter2D(Collision2D collisionData)
    {
        Collider2D objectWeCollidedWith = collisionData.collider;

        //get player health script 
        PlayerHealth player = objectWeCollidedWith.GetComponent<PlayerHealth>();

        // Check if we actually found a player health script
        // This if statement is true if the player variable is NOT null
        if (player != null)
        {
            // This means there WAS a PlayerHealth script attached to the object we bumped into
            // Which means this object is indeed the player
            player.changeHealth(-hazardDamage);
            Destroy(gameObject); //destroys the enemy

        }

        
    }
}
