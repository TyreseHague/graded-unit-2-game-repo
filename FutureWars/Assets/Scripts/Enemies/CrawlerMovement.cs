﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlerMovement : MonoBehaviour
{
    //Attack Variables
    public float moveSpeed;     // How fast we move
    public Transform player;        // The thing you want to chase
    public float agroRange;   // How close must the player be to track
    private Rigidbody2D rb2d;   // The rigidbody attached to this object

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>(); // Get the rigidbody that we'll be using for movement

    }


    // Update is called once per frame
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position); //calculates the distance between the player and the enemy


        if (distToPlayer < agroRange) //if the disance is further from the target then...
        {
            ChasePlayer(); //calls function
        }
        else
        {
            //stop chasing
            StopChasingPlayer(); //calls function
        }

    }


    private void ChasePlayer()
    {
        if (transform.position.x < player.position.x)
        {
            rb2d.velocity = new Vector2(moveSpeed, 0); //moves the enemy right
            transform.localScale = new Vector2(-0.35f, 0.35f); //transform position to right
        }
        else
        {
            rb2d.velocity = new Vector2(-moveSpeed, 0); //moves the enemy left
            transform.localScale = new Vector2(0.35f, 0.35f); //transform position to left
        }
    }

    private void StopChasingPlayer() //new function
    {
        rb2d.velocity = new Vector2(0, 0);//no movement
    }
}
