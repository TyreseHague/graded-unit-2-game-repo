﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    //health ints
    public int startingHealth;
    public int currentHealth;    //sets what the health currently is

    //materials
    private Material matWhite;
    private Material matDefault;
    SpriteRenderer sr;

    //door parts
    public bool isSwitch = false; //boolean for if the enemy is a switch for a door (off naturally)
    public bool isSwitch2 = false; //boolean for if the enemy is a switch for a different type of door (off naturally)
    public GameObject door; //sets the door object as a public gameobject


    //this is called before the start function
    void Awake()
    {
        //make current health equal to starting health
        currentHealth = startingHealth;

        sr = GetComponent<SpriteRenderer>(); //sr is the enemy
        matWhite = Resources.Load("WhiteFlash", typeof(Material)) as Material; //the matWhite is a white image in the assets folder
        matDefault = sr.material; //the default is the normal sprite
    }




    public void hurtEnemy(int damageTooGive)
    {
        currentHealth -= damageTooGive; //minus the damage
        sr.material = matWhite; //enemy turns white
        
        if (currentHealth <= 0 ) //if the player has 0 health 
        {
            Destroy(gameObject); //destroys enemy

            if (isSwitch == true) //if the enemy is marked as a "switch"
            {
                door.transform.position += new Vector3(0, 2); //moves the door object upwards
            }
            if (isSwitch2 == true) //if the enemy is marked as a "switch2"
            {
                door.transform.position += new Vector3(-2, 0); //moves the door object to the left
            }
        }
        else
        {
            Invoke("ResetMaterial", .1f); //runs  reset material after 1 frame
        }}

    void ResetMaterial()
    {
        sr.material = matDefault; //resets the material
    }


    public void setMaxHealth()
    {
        currentHealth -= startingHealth; //sets the current health to match the starting health
    }

}
