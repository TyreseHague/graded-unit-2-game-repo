﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLineMovement : MonoBehaviour
{
    //public variables
    public float forceStrength; //how fast the script pushes the enemy 
    public float distanceFromTarget;   // How close must the player be to track

    public Vector2 direction; //what direction the enemy is moving
    public Transform target; //finds player


    //private variables 
    private Rigidbody2D ourRigidbody; //container for rigidbody (currently empty)


    // Awake is called when the script is first loaded
    void Awake()
    {
        //Get and store the rigidbody we'll be using for movement
        ourRigidbody = GetComponent<Rigidbody2D>();

        //Normalise oure direction
        //Normalise changes it to be length 1, so it can multiply by the spped/force
        direction = direction.normalized;
    }


    // Update is called once per frame
    void Update()
    {

        float distance = ((Vector2)target.position - (Vector2)transform.position).magnitude; //finds distance from target

        if (distance <= distanceFromTarget) //if the disance is further from the target then...
        {
            //Move in the correct direction with the correct direction with the set force strength
            ourRigidbody.AddForce(direction * forceStrength);
        }


    }
}
