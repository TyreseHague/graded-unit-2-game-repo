﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public GameObject door; //sets door as a public game object
    bool isOpened = false; //makes a boolean to see if the door is opened 



    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") == true) //checks if the object collided with is the player
            if (isOpened == false) //if the door isnt open
        door.transform.position += new Vector3(0, 2); //moves the door up by 2 
        isOpened = true; //sets door to open
    }



    
}
